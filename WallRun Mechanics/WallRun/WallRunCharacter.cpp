// Copyright Epic Games, Inc. All Rights Reserved.

#include "WallRunCharacter.h"
#include "WallRunProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId
#include "GameFramework/CharacterMovementComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);


AWallRunCharacter::AWallRunCharacter()
{
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;
	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	GunOffset = FVector(100.0f, 0.0f, 10.0f);

}

void AWallRunCharacter::Tick(float DeltaSec)
{
	Super::Tick(DeltaSec);
	if(IsWallRunning)
		UpdateWallRun();
	CameraTiltTimeline.TickTimeline(DeltaSec);
}

void AWallRunCharacter::BeginPlay()
{

	Super::BeginPlay();

	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	GetCapsuleComponent()->OnComponentHit.AddDynamic(this, &AWallRunCharacter::OnPlayerCapsuleHit);
	GetCharacterMovement()->SetPlaneConstraintEnabled(true);

	if (IsValid(CameraTiltCurve))
	{
		FOnTimelineFloat TimelineCallBack;
		TimelineCallBack.BindUFunction(this, FName("UpdateCameraTilt"));
		CameraTiltTimeline.AddInterpFloat(CameraTiltCurve, TimelineCallBack);
	}
}

void AWallRunCharacter::Jump()
{
	if (IsWallRunning)
	{
		//
		FVector JumpDir = FVector::ZeroVector;
		if (CurrentWallRunSide == EWallRunSide::Right)
		{
			JumpDir = FVector::CrossProduct(CurrenWallRunDir, FVector::UpVector).GetSafeNormal();
		}
		else
		{
			JumpDir = FVector::CrossProduct(FVector::UpVector, CurrenWallRunDir).GetSafeNormal();
		}
		JumpDir += FVector::UpVector;
		LaunchCharacter(GetCharacterMovement()->JumpZVelocity * JumpDir.GetSafeNormal(), false, true);
		StopWallRun();
	}
	else
	{
		Super::Jump();
	}

}

void AWallRunCharacter::OnPlayerCapsuleHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{

	FVector HitNormal = Hit.ImpactNormal;
	FVector Dir = FVector::ZeroVector;
	if (!IsSurfaceWallRunable(HitNormal))
		return;

	if (!GetCharacterMovement()->IsFalling())
		return;

	EWallRunSide Side = EWallRunSide::None;
	FVector Direction = FVector::ZeroVector;
	GetWallRunSideAndDirection(HitNormal, Side, Direction);

	if (!AreRequiredKeysDown(Side))
		return;

	StartWallRun(Side, Direction);
}

void AWallRunCharacter::GetWallRunSideAndDirection(const FVector &HitNormal, EWallRunSide &Side, FVector &Dir) const
{
	if (FVector::DotProduct(HitNormal, GetActorRightVector()) > 0)
	{
		Side = EWallRunSide::Left;
		Dir = FVector::CrossProduct(HitNormal, FVector::UpVector).GetSafeNormal();
		
	}
	else
	{
		Side = EWallRunSide::Right;
		Dir = FVector::CrossProduct(FVector::UpVector, HitNormal).GetSafeNormal();
	
	}
}

bool AWallRunCharacter::IsSurfaceWallRunable(const FVector & SurfaceNormal) const
{
	if (SurfaceNormal.Z > GetCharacterMovement()->GetWalkableFloorZ() || SurfaceNormal.Z < -0.005f)
		return false;

	return true;
}

bool AWallRunCharacter::AreRequiredKeysDown(EWallRunSide Side) const
{
	if (ForwardAxis < 0.1f)
		return false;
	if (Side == EWallRunSide::Right && RightAxis < -0.1f)
		return false;
	if (Side == EWallRunSide::Left && RightAxis > 0.1f)
		return false;

	return true;
}

void AWallRunCharacter::StartWallRun(EWallRunSide Side, const FVector & Dir)
{
	BeginCameraTilt();

	IsWallRunning = true;
	CurrentWallRunSide = Side;
	CurrenWallRunDir = Dir;

	GetCharacterMovement()->SetPlaneConstraintNormal(FVector::UpVector);

	GetWorld()->GetTimerManager().SetTimer(WallRunTimer, this, &AWallRunCharacter::StopWallRun, MaxWallRunTime, false);
}

void AWallRunCharacter::StopWallRun()
{
	EndCameraTilt();
	IsWallRunning = false;
	/*CurrentWallRunSide = EWallRunSide::None;
	CurrenWallRunDir = FVector::ZeroVector;*/

	GetCharacterMovement()->SetPlaneConstraintNormal(FVector::ZeroVector);
}

void AWallRunCharacter::UpdateWallRun()
{

	if (!AreRequiredKeysDown(CurrentWallRunSide))
	{
		StopWallRun();
		return;
	}

	FHitResult HitResult;
	FVector LineTraceDir = CurrentWallRunSide == EWallRunSide::Left ? GetActorRightVector() : -GetActorRightVector();
	float LineTraceLenght = 200.f;
	float LineTraceLenght1 = -200.f;
	FVector StartPos = GetActorLocation();
	FVector EndPos = StartPos + LineTraceLenght + LineTraceDir;
	FVector EndPos1 = StartPos + LineTraceLenght1 + LineTraceDir;

	FCollisionQueryParams QueryParam;
	QueryParam.AddIgnoredActor(this);

	if (GetWorld()->LineTraceSingleByChannel(HitResult, StartPos, EndPos, ECC_Visibility, QueryParam))
	{

		EWallRunSide Side = EWallRunSide::None;
		FVector Dir = FVector::ZeroVector;
		GetWallRunSideAndDirection(HitResult.ImpactNormal, Side, Dir);

		if (Side != CurrentWallRunSide)
			StopWallRun();
		else
		{
			CurrenWallRunDir = Dir;
			GetCharacterMovement()->Velocity = GetCharacterMovement()->GetMaxSpeed() * CurrenWallRunDir;
		}
	}

	else if (GetWorld()->LineTraceSingleByChannel(HitResult, StartPos, EndPos1, ECC_Visibility, QueryParam))
	{

			EWallRunSide Side = EWallRunSide::None;
			FVector Dir = FVector::ZeroVector;
			GetWallRunSideAndDirection(HitResult.ImpactNormal, Side, Dir);

			if (Side != CurrentWallRunSide)
				StopWallRun();
			else
			{
				CurrenWallRunDir = Dir;
				GetCharacterMovement()->Velocity = GetCharacterMovement()->GetMaxSpeed() * CurrenWallRunDir;
			}
	}
	else 
		StopWallRun();
}

void AWallRunCharacter::UpdateCameraTilt(float value)
{
	FRotator CurrentControlRotation = GetControlRotation();
	CurrentControlRotation.Roll = (CurrentWallRunSide == EWallRunSide::Left ? value : -value);
	GetController()->SetControlRotation(CurrentControlRotation);
}


void AWallRunCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AWallRunCharacter::OnFire);

	//PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AWallRunCharacter::OnResetVR);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AWallRunCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AWallRunCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AWallRunCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AWallRunCharacter::LookUpAtRate);
}

void AWallRunCharacter::OnFire()
{
	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			const FRotator SpawnRotation = GetControlRotation();
				
			const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

			World->SpawnActor<AWallRunProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
		}
	}

	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	if (FireAnimation != NULL)
	{
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

void AWallRunCharacter::MoveForward(float Value)
{
	ForwardAxis = Value;
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AWallRunCharacter::MoveRight(float Value)
{
	RightAxis = Value;
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AWallRunCharacter::TurnAtRate(float Rate)
{
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AWallRunCharacter::LookUpAtRate(float Rate)
{
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

