// Copyright Epic Games, Inc. All Rights Reserved.

#include "FPSGameGameMode.h"
#include "FPSGameHUD.h"
#include "FPSGameCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "FPSGameStateBase.h"

AFPSGameGameMode::AFPSGameGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	/*static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/FirstPersonCPP/Blueprints/BP_Character.BP_Character_C"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;*/

	// use our custom HUD class
	HUDClass = AFPSGameHUD::StaticClass();
	GameStateClass = AFPSGameStateBase::StaticClass();
}

void AFPSGameGameMode::CompleteMission(APawn* InstigatorPawn, bool bMissionSuccess)
{
	if (InstigatorPawn)
	{
		if (SpectatingViewPointClass)
		{
			TArray<AActor*> ReturnedActors;
			UGameplayStatics::GetAllActorsOfClass(this, SpectatingViewPointClass, ReturnedActors);
					
			if (ReturnedActors.Num() > 0)
			{
				AActor* NewViewTarget = ReturnedActors[0];

				for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
				{
					APlayerController* PC = It->Get();
					if (PC)
					{
						PC->SetViewTargetWithBlend(NewViewTarget, 0.5, EViewTargetBlendFunction::VTBlend_Cubic);
					}
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("*"));
		}
	}

	AFPSGameStateBase* GS = GetGameState<AFPSGameStateBase>();
	if (GS)
		GS->MulticastOnMissionComplete(InstigatorPawn, bMissionSuccess);

	OnMissionCompleted(InstigatorPawn, bMissionSuccess);
}
