// Fill out your copyright notice in the Description page of Project Settings.


#include "FPS_AI_Guards.h"
#include "Perception/PawnSensingComponent.h"
#include "DrawDebugHelpers.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NavigationSystem.h"
#include "Net/UnrealNetwork.h"
#include "FPSGameGameMode.h"

AFPS_AI_Guards::AFPS_AI_Guards()
{
	PrimaryActorTick.bCanEverTick = true;

	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));

	/*PawnSensingComp->OnSeePawn.AddDynamic(this, &AFPS_AI_Guards::OnPawnSeen);
	PawnSensingComp->OnHearNoise.AddDynamic(this, &AFPS_AI_Guards::OnNoiseHeard);*/

	GuardState = EAIState::Idle;
}

void AFPS_AI_Guards::BeginPlay()
{
	Super::BeginPlay();

	PawnSensingComp->OnSeePawn.AddDynamic(this, &AFPS_AI_Guards::OnPawnSeen);
	PawnSensingComp->OnHearNoise.AddDynamic(this, &AFPS_AI_Guards::OnNoiseHeard);

	OriginalRotaiton = GetActorRotation();

	if (bPatrol)
		MoveToNextPatrolPoint();
}

void AFPS_AI_Guards::OnPawnSeen(APawn* SeePawn)
{
	if (SeePawn == nullptr)
	{
		return;
	}
	DrawDebugSphere(GetWorld(), SeePawn->GetActorLocation(), 32.0f, 12, FColor::Green, false, 10.0f);

	AFPSGameGameMode* GM = Cast<AFPSGameGameMode>(GetWorld()->GetAuthGameMode());
	if (GM)
		GM->CompleteMission(SeePawn, false);

	SetGuardState(EAIState::Alerted);
}

void AFPS_AI_Guards::OnNoiseHeard(APawn* NoiseInstigator, const FVector& Location, float Volume)
{
	DrawDebugSphere(GetWorld(), Location, 32.0f, 12, FColor::Red, false, 10.0f);

	FVector Direction = Location - GetActorLocation();
	Direction.Normalize();

	FRotator NewLookAt = FRotationMatrix::MakeFromX(Direction).Rotator();
	SetActorRotation(NewLookAt);

	AController* controller = GetController();
	if (controller)
		controller->StopMovement();
}

// Called every frame
void AFPS_AI_Guards::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (CurrentPatrolPoint)
	{
		FVector Delta = GetActorLocation() - CurrentPatrolPoint->GetActorLocation();
		float DistanceToGoal = Delta.Size();

		if (DistanceToGoal < 50)
			MoveToNextPatrolPoint();
	}
}

void AFPS_AI_Guards::ResetOrientation()
{
	if (GuardState == EAIState::Alerted)
		return;
	SetActorRotation(OriginalRotaiton);

	SetGuardState(EAIState::Idle);

	if (bPatrol)
		MoveToNextPatrolPoint();
}

void AFPS_AI_Guards::OnRep_GuardState()
{
	OnStateChanged(GuardState);
}

void AFPS_AI_Guards::SetGuardState(EAIState NewState)
{
	if (GuardState == NewState)
		return;

	GuardState = NewState;
	OnRep_GuardState();
}


void AFPS_AI_Guards::MoveToNextPatrolPoint()
{
	if (CurrentPatrolPoint == nullptr || CurrentPatrolPoint == SecondPatrolPoint)
		CurrentPatrolPoint = FirstPatrolPoint;
	else
		CurrentPatrolPoint = SecondPatrolPoint;

	UAIBlueprintHelperLibrary::SimpleMoveToActor(GetController(), CurrentPatrolPoint);
}

void AFPS_AI_Guards::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
		
	DOREPLIFETIME(AFPS_AI_Guards, GuardState);
}
