#include "Brick.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Ball.h"

// Sets default values
ABrick::ABrick()
{
	PrimaryActorTick.bCanEverTick = true;

	SM_Brick = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Brick"));

	SM_Brick->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	Box_Collision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	Box_Collision->SetBoxExtent(FVector(25.0f, 10.0f, 10.0f));

	RootComponent = Box_Collision;
}

// Called when the game starts or when spawned
void ABrick::BeginPlay()
{
	Super::BeginPlay();
	Box_Collision->OnComponentBeginOverlap.AddDynamic(this, &ABrick::OnOverlapBegin);
}

void ABrick::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABrick::OnOverlapBegin(UPrimitiveComponent * OverLappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp,
	int32 OtherBodyIndexType, bool bFromSweep, const FHitResult & SweepRes)
{
	if (OtherActor->ActorHasTag("Ball"))
	{
		ABall* MyBall = Cast<ABall>(OtherActor);

		/// crush ///

		//FVector BallVelosity = MyBall->GetVelocity();
		//BallVelosity = BallVelosity * (SpeedModifierOnBounse - 1.0f);
		//MyBall->GetBall()->SetPhysicsLinearVelocity(BallVelosity, true);

		////////////

		FTimerHandle UnusedHandle;
		GetWorldTimerManager().SetTimer(UnusedHandle, this, &ABrick::DestroyBrick, 0.1, false);
	}

}

void ABrick::DestroyBrick()
{
	this->Destroy();
}


