// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusPoints.h"
#include "Kismet/GameplayStatics.h"
#include "SpaceArcadeGameModeBase.h"

void ABonusPoints::BonusCollected_Implementation()
{
	ASpaceArcadeGameModeBase* GameMode = Cast<ASpaceArcadeGameModeBase>(UGameplayStatics::GetGameMode(this));
	if (GameMode)
		GameMode->AddPoints(Points);

	Super::BonusCollected_Implementation();
}