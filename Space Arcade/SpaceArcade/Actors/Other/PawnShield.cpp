// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnShield.h"
#include "Pawns/PlayerPawn.h"
#include "Engine/World.h"
#include "TimerManager.h"

APawnShield::APawnShield() : ShieldTime(5.f)
{

}

void APawnShield::ActivateShield(APlayerPawn * PlayerPawn)
{
	if (!PlayerPawn)
	{
		Destroy();
		return;
	}
	ShildForPawn = PlayerPawn;
	PlayerPawn->bCanBeDamaged = false;
	FAttachmentTransformRules AttachRulles = FAttachmentTransformRules(EAttachmentRule::SnapToTarget,
		EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, false);
	AttachToActor(PlayerPawn, AttachRulles);
	GetWorld()->GetTimerManager().SetTimer(ShieldTimer, this, &APawnShield::DeactivateShield, ShieldTime, false);
}

void APawnShield::DeactivateShield()
{
	if (!ShildForPawn) return;
	ShildForPawn->bCanBeDamaged = true;
	Destroy();
}
