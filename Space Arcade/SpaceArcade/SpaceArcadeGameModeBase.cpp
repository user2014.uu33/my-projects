
#include "SpaceArcadeGameModeBase.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"
#include "Pawns/PlayerPawn.h"

ASpaceArcadeGameModeBase::ASpaceArcadeGameModeBase(): PlayerRecoverTime(3), CurrentShootLevel(-1),
IncreaseDificultyPer(4.f)
{
	EnemySpawnController = CreateDefaultSubobject<UEnemySpawnController>(TEXT("EnemySpawnController"));
	HealthsComponent = CreateDefaultSubobject<UGameTryingHealthComponent>(TEXT("HealthComponent"));
}

void ASpaceArcadeGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	HealthsComponent->HealthsEnded.AddDynamic(this, &ASpaceArcadeGameModeBase::EndGame);
	PlayerPawn = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (!PlayerPawn)
		return;

	ChangeShootLevel(true);

	PlayerPawn->PawnDamaged.AddDynamic(this, &ASpaceArcadeGameModeBase::ExplodePawn);
	GetWorld()->GetTimerManager().SetTimer(IncreaseDificultyTimer, this, &ASpaceArcadeGameModeBase::IncreaseDificulty, IncreaseDificultyPer, true);
}

void ASpaceArcadeGameModeBase::ExplodePawn_Implementation()
{
	PlayerPawn->ExplodePawn();
	HealthsComponent->ChangeHelath(-1);
	ChangeShootLevel(false);
	if (!IsGameOver)
		GetWorld()->GetTimerManager().SetTimer(RecoverTimer, this, &ASpaceArcadeGameModeBase::RecoverPawn, PlayerRecoverTime, false);
}

void ASpaceArcadeGameModeBase::RecoverPawn_Implementation()
{
	PlayerPawn->RecoverPawn();
}

void ASpaceArcadeGameModeBase::EndGame()
{
	IsGameOver = true;
	EnemySpawnController->SetActive(false);
	GameOver.Broadcast();
	UGameplayStatics::GetPlayerPawn(this, 0)->Destroy();
	SetPause(UGameplayStatics::GetPlayerController(this, 0), false);//
}

void ASpaceArcadeGameModeBase::IncreaseDificulty()
{
	EnemySpawnController->ChangeStageTimeM = FMath::Max(EnemySpawnController->ChangeStageTimeM * 0.95, 0.4);
}

bool ASpaceArcadeGameModeBase::ChangeShootLevel(bool Up)
{
	PlayerPawn = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(this, 0));//
	if (!PlayerPawn) return false;

	int NewLevel = FMath::Clamp(CurrentShootLevel + (Up ? 1 : -1), 0, ShootInfoLevels.Num() - 1);

	if (NewLevel == CurrentShootLevel) return false;

	CurrentShootLevel = NewLevel;

	PlayerPawn->ShootComponent->ShootInfos = ShootInfoLevels[CurrentShootLevel].ShootInfos;
	PlayerPawn->ShootComponent->ShootPeriod = ShootInfoLevels[CurrentShootLevel].ShootPeriod;

	return true;
}

void ASpaceArcadeGameModeBase::AddPoints(int Points)
{
	GamePoints += Points;
}
