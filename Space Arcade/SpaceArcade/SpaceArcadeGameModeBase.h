// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Components/GameTryingHealthComponent.h"
#include "Components/EnemySpawnController.h"
#include "Components/ShootComponent.h"
#include "SpaceArcadeGameModeBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGameOverEvent);

USTRUCT(BlueprintType)
struct FShootInfoLevel
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
		TArray<FShootInfo> ShootInfos;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
		float ShootPeriod;
};

UCLASS()
class SPACEARCADE_API ASpaceArcadeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	ASpaceArcadeGameModeBase();
	virtual void BeginPlay() override;
protected:

	UFUNCTION(BlueprintNativeEvent, Category = "Game")
		void ExplodePawn();
	void ExplodePawn_Implementation();

	UFUNCTION(BlueprintNativeEvent, Category = "Game")
		void RecoverPawn();
	void RecoverPawn_Implementation();

	FTimerHandle RecoverTimer;
	FTimerHandle IncreaseDificultyTimer;

	bool IsGameOver;
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Enemies")
		UEnemySpawnController* EnemySpawnController;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game Healths")
		UGameTryingHealthComponent* HealthsComponent;

	UPROPERTY(BlueprintAssignable, Category = "Game")
		FGameOverEvent GameOver;

	UFUNCTION(BlueprintCallable, Category = "Game")
		void EndGame();

	UFUNCTION(BlueprintCallable, Category = "Game")
		void IncreaseDificulty();

	UFUNCTION(BlueprintCallable, Category = "Game")
		void AddPoints(int Points);

	UFUNCTION(BlueprintCallable, Category = "Game")
		bool ChangeShootLevel(bool Up);

	UPROPERTY(BlueprintReadWrite, Category = "Game")
		float PlayerRecoverTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game")
		float IncreaseDificultyPer;

	UPROPERTY(BlueprintReadOnly, Category = "Game")
		class APlayerPawn* PlayerPawn;

	UPROPERTY(BlueprintReadOnly, Category = "Game")
		int GamePoints;

	UPROPERTY(EditDefaultsOnly, Category = "Shooting")
		TArray<FShootInfoLevel> ShootInfoLevels;

	UPROPERTY(BlueprintReadOnly, Category = "Shooting")
		int CurrentShootLevel;
};
