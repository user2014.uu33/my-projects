// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameTryingHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHealthsEndedEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHealthsChangedEvent, int, ChangeValue);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SPACEARCADE_API UGameTryingHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UGameTryingHealthComponent();

protected:
	
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, Category = "GameHealth")
	int Healths;
public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	UFUNCTION(BlueprintCallable, Category = "GameHealth")
		void ChangeHelath(int value);

	UFUNCTION(BlueprintPure, Category = "GameHealth")
		int GetHealths();

	UPROPERTY(BlueprintAssignable, Category = "GameHealth")
		FHealthsEndedEvent HealthsEnded;

	UPROPERTY(BlueprintAssignable, Category = "GameHealth")
		FHealthsChangedEvent HealthsChanged;
};
