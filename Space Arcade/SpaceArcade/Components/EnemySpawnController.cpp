
#include "EnemySpawnController.h"
#include "Engine/World.h"
#include "TimerManager.h"

UEnemySpawnController::UEnemySpawnController() : ChangeStageTimeM(2.f)
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UEnemySpawnController::BeginPlay()
{
	Super::BeginPlay();
	Rand.GenerateNewSeed();
	StartSpawnStage();
}

void UEnemySpawnController::StartSpawnStage()
{
	SpawnStage = SpawnStages[Rand.RandRange(0, SpawnStages.Num() - 1)];
	EnemiesSpawned = 0;
	SpawnEnemy();
	float ChangeStageTime = Rand.RandRange(StageMinDelay, StageMaxDelay) * ChangeStageTimeM;

	GetWorld()->GetTimerManager().SetTimer(ChangeStageTimer, this, 
		&UEnemySpawnController::StartSpawnStage, Rand.RandRange(StageMinDelay, StageMaxDelay), false);
}
 
void UEnemySpawnController::SpawnEnemy()
{
	FActorSpawnParameters SpawnParameters;
	GetWorld()->SpawnActor<AEnemyPawn>(SpawnStage.EnemyClass, SpawnStage.SpawnTransform, SpawnParameters);

	EnemiesSpawned++;
	if (EnemiesSpawned < SpawnStage.NumOfEnemies) {
		GetWorld()->GetTimerManager().SetTimer(EnemySpawnTimer, this, &UEnemySpawnController::SpawnEnemy, SpawnStage.SpawnDelay, false);
	}
}

void UEnemySpawnController::Deactivate()
{
	Super::Deactivate();
	GetWorld()->GetTimerManager().ClearTimer(ChangeStageTimer);
	GetWorld()->GetTimerManager().ClearTimer(EnemySpawnTimer);
}




