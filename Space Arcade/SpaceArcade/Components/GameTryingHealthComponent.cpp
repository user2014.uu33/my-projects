
#include "GameTryingHealthComponent.h"
#include "GameFramework/Pawn.h"
#include "Kismet/GameplayStatics.h"

UGameTryingHealthComponent::UGameTryingHealthComponent() : Healths(3)
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UGameTryingHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	APawn *PlayerPawn = UGameplayStatics::GetPlayerPawn(this, 0);

	if (!PlayerPawn) {
		UE_LOG(LogTemp, Error, TEXT("No Player Pawn!"));
		return;
	}
}

void UGameTryingHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
}

void UGameTryingHealthComponent::ChangeHelath(int value)
{
	Healths += value;
	HealthsChanged.Broadcast(value);

	if (Healths <= 0) {
		HealthsEnded.Broadcast();
	}
	UE_LOG(LogTemp, Log, TEXT("Helth Chenged: %i"), Healths);
}

int UGameTryingHealthComponent::GetHealths()
{
	return Healths;
}

